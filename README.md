## Projekt im lokalen Maven-Repo ablegen
Um das Projekt auch in anderen Projekten zu verwenden, sollte ein Artefakt gebaut werden, 
das im lokalen Repository abgelegt werden muss. 
``` 
mvn clean install
```
Dabei werden drei Artefakte erzeugt:
* Bibliothek
* Javadoc
* Quellcode
