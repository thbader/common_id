package sh.bader.common.id;

import java.io.Serializable;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.lang3.StringUtils;

public interface ID<I extends ID<I, E>, E extends IDEntity<I, E>> extends Serializable, Comparable<I> {

    /**
     * Get the entity class of the sid.
     *
     * @return the entity class.
     */
    Class<E> getEntityClass();

    /**
     * Get the identifier of the entity.
     *
     * @return the identifier of the entity.
     */
    UUID getId();

    static <I extends ID<I, E>, E extends IDEntity<I, E>> I forEntityClass(final Class<E> entityClass, final UUID uuid) {
        Objects.requireNonNull(entityClass);

        if (!IDEntity.class.isAssignableFrom(entityClass)) {
            throw new IllegalArgumentException("not an entity class: " + entityClass);
        }

        Class<I> idClass = Arrays.stream(entityClass.getDeclaredMethods())
                .filter(m -> m.getName().equals("getId"))
                .map(Method::getReturnType)
                .filter(ID.class::isAssignableFrom)
                .filter(c -> !c.isInterface())
                .map(c -> (Class<I>) c)
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException("unknown entity class: " + entityClass));

        return forIdClass(idClass, uuid);
    }

    static <I extends ID<I, E>, E extends IDEntity<I, E>> I forIdClass(final Class<I> idClass, final UUID uuid) {
        Objects.requireNonNull(idClass);

        if (uuid == null) {
            return null;
        }

        if (!ID.class.isAssignableFrom(idClass)) {
            throw new IllegalArgumentException("not an id class: " + idClass);
        }

        Constructor<I> constructor = Stream.of(idClass.getConstructors())
                .map(c -> (Constructor<I>) c)
                .filter(c -> c.getParameterTypes().length == 1)
                .filter(c -> UUID.class.isAssignableFrom(c.getParameterTypes()[0]))
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException("constructor not found for class: " + idClass));

        try {
            return constructor.newInstance(uuid);
        } catch (final ReflectiveOperationException | SecurityException | IllegalArgumentException e) {
            throw new IllegalArgumentException("creating id failed: class=" + idClass + ", value=" + uuid, e);
        }
    }

    static <I extends ID<I, E>, E extends IDEntity<I, E>> I forEntityClass(final Class<E> entityClass, final String uuidString) {
        UUID uuid = Optional.of(StringUtils.trimToNull(uuidString))
                .map(UUID::fromString)
                .orElse(null);

        return forEntityClass(entityClass, uuid);
    }

    static <I extends ID<I, E>, E extends IDEntity<I, E>> I forIdClass(final Class<I> clazz, final String uuidString) {
        UUID uuid = Optional.of(StringUtils.trimToNull(uuidString))
                .map(UUID::fromString)
                .orElse(null);

        return forIdClass(clazz, uuid);
    }

    static <I extends ID<I, E>, E extends IDEntity<I, E>> Optional<I> getId(final IDEntity<I, E> entity) {
        if (entity == null) {
            return Optional.empty();
        }

        return Optional.of(entity.getId());
    }

    static <I extends ID<I, E>, E extends IDEntity<I, E>> Set<I> getIds(Collection<? extends IDEntity<I, E>> entities) {
        if (entities == null) {
            return Collections.emptySet();
        }

        return entities.stream()
                .map(ID::getId)
                .filter(Optional::isPresent)
                .map(Optional::get)
                .collect(Collectors.toUnmodifiableSet());
    }
}
