package sh.bader.common.id;

import java.io.Serializable;
import java.util.Objects;

import org.apache.commons.lang3.builder.CompareToBuilder;

/**
 * Base interface for all entities.
 */
public interface IDEntity<I extends ID<I, E>, E extends IDEntity<I, E>> extends Comparable<E>, Serializable {
    I getId();

    @Override
    default int compareTo(E o) {
        return new CompareToBuilder()
                .append(this.getId(), o.getId())
                .toComparison();
    }

    static boolean equalsById(IDEntity<?,?> entity, Object object) {
        if (entity == object) {
            return true;
        }

        if (object== null || !IDEntity.class.isAssignableFrom(object.getClass())) {
            return false;
        }

        return Objects.equals(
            entity.getId(),
            ((IDEntity<?,?>)object).getId()
        );
    }

    static int hashCodeById(IDEntity<?,?> entity) {
        return Objects.hash(entity.getId());
    }
}
