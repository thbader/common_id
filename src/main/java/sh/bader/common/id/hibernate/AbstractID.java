package sh.bader.common.id.hibernate;

import jakarta.persistence.Column;
import jakarta.persistence.MappedSuperclass;
import jakarta.persistence.Transient;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import sh.bader.common.id.IDEntity;
import sh.bader.common.id.ID;

import java.util.Objects;
import java.util.UUID;

import static java.util.Objects.requireNonNull;

@MappedSuperclass
public abstract class AbstractID<I extends ID<I, E>, E extends IDEntity<I, E>> implements ID<I, E> {
    private static final ToStringStyle ID_TO_STRING_STYLE = new IdToStringStyle();
    
    private static final long serialVersionUID = -3481426265140195126L;

    @Transient
    private final Class<E> entityClass;

    @Column(name = "id", updatable = false, nullable = false, unique = true)
    private final UUID id;

    public AbstractID(final Class<E> entityClass) {
        this(entityClass, null);
    }

    public AbstractID(final Class<E> entityClass, final UUID id) {
        this.entityClass = requireNonNull(entityClass);
        this.id = id;
    }

    @Override
    public Class<E> getEntityClass() {
        return this.entityClass;
    }

    @Override
    public UUID getId() {
        return this.id;
    }

    @Override
    public final boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || !this.getClass().equals(obj.getClass())) {
            return false;
        }

        final AbstractID<?, ?> that = (AbstractID<?, ?>) obj;
        return new EqualsBuilder()
                .append(this.getEntityClass(), that.getEntityClass())
                .append(this.getId(), that.getId())
                .isEquals();
    }

    @Override
    public final int hashCode() {
        return Objects.hash(
                this.entityClass,
                this.id
        );
    }

    @Override
    public final String toString() {
        return new ToStringBuilder(this, ID_TO_STRING_STYLE)
                .append(this.getId())
                .toString();
    }

    @Override
    public int compareTo(I o) {
        return this.id.compareTo(o.getId());
    }

    private static class IdToStringStyle extends ToStringStyle {
        private static final long serialVersionUID = 1L;

        /**
         * <p>Constructor.</p>
         *
         * <p>Use the static constant rather than instantiating.</p>
         */
        IdToStringStyle() {
            super();
            this.setUseShortClassName(true);
            this.setUseIdentityHashCode(false);
            this.setUseFieldNames(false);
        }

        /**
         * <p>Ensure <code>Singleton</ode> after serialization.</p>
         *
         * @return the singleton
         */
        private Object readResolve() {
            return ID_TO_STRING_STYLE;
        }
    }
}
