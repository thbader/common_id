package sh.bader.common.id.hibernate;

import org.hibernate.HibernateException;
import org.hibernate.MappingException;
import org.hibernate.engine.spi.SharedSessionContractImplementor;
import org.hibernate.id.IdentifierGenerator;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.type.Type;
import sh.bader.common.id.ID;

import java.io.Serializable;
import java.util.Properties;
import java.util.UUID;

public class IDGenerator implements IdentifierGenerator {
    @Override
    public void configure(Type type, Properties params, ServiceRegistry serviceRegistry) throws MappingException {
        if (!ID.class.isAssignableFrom(type.getReturnedClass())) {
            throw new MappingException("invalid type: " + type.getReturnedClass());
        }
    }

    @Override
    public Serializable generate(SharedSessionContractImplementor session, Object object) throws HibernateException {
        return ID.forEntityClass((Class) object.getClass(), UUID.randomUUID());
    }
}
