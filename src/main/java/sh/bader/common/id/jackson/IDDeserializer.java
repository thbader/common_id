package sh.bader.common.id.jackson;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.exc.InvalidFormatException;
import sh.bader.common.id.ID;

import java.io.IOException;
import java.util.UUID;

public final class IDDeserializer extends JsonDeserializer<ID<?, ?>> {

    private final Class idClass;

    public IDDeserializer(Class idClass) {
        this.idClass = idClass;
    }

    @Override
    public ID<?, ?> deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
        String uuid = jsonParser.getText();
        try {
            return ID.forIdClass(this.idClass, uuid);
        } catch (IllegalArgumentException e) {
            throw new InvalidFormatException(jsonParser, "not a valid UUID value", uuid, UUID.class);
        }
    }
}
