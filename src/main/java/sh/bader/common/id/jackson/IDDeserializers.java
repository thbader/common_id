package sh.bader.common.id.jackson;

import com.fasterxml.jackson.databind.BeanDescription;
import com.fasterxml.jackson.databind.DeserializationConfig;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.deser.Deserializers;
import com.fasterxml.jackson.databind.jsontype.TypeDeserializer;
import com.fasterxml.jackson.databind.type.ReferenceType;
import sh.bader.common.id.ID;

import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

final class IDDeserializers extends Deserializers.Base {

    private final Map<Class<? extends ID<?, ?>>, IDDeserializer> deserializers;

    IDDeserializers(Set<Class<? extends ID<?, ?>>> idClasses) {
        this.deserializers = idClasses.stream().collect(Collectors.toMap(
                idClass -> idClass,
                IDDeserializer::new)
        );
    }

    @Override
    public JsonDeserializer<?> findReferenceDeserializer(
            ReferenceType refType,
            DeserializationConfig config,
            BeanDescription beanDesc,
            TypeDeserializer contentTypeDeserializer,
            JsonDeserializer<?> contentDeserializer
    ) {
        return this.deserializers.get(refType.getRawClass());
    }
}
