package sh.bader.common.id.jackson;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.Version;
import com.fasterxml.jackson.databind.Module;
import org.reflections.Reflections;
import sh.bader.common.id.ID;

import java.lang.reflect.Modifier;
import java.util.Set;
import java.util.stream.Collectors;

public class IDModule extends Module {
    final Set<Class<? extends ID<?, ?>>> idClasses;

    public IDModule(Class<? extends ID<?, ?>> baseClass) {
        this(baseClass.getPackage().getName());
    }

    private IDModule(String prefix) {
        final Reflections reflections = new Reflections(prefix);

        this.idClasses = reflections.getSubTypesOf(ID.class)
                .stream()
                .filter(clazz -> !Modifier.isAbstract(clazz.getModifiers()))
                .map(clazz -> (Class<ID<?, ?>>) clazz)
                .collect(Collectors.toSet());
    }

    @Override
    public String getModuleName() {
        return "Id";
    }

    @Override
    public Version version() {
        return Version.unknownVersion();
    }

    @Override
    public void setupModule(SetupContext context) {
        // deserialization
        context.addDeserializers(new IDDeserializers(this.idClasses));
        context.addTypeModifier(new IDTypeModifier());

        // serialization
        context.addSerializers(new IDSerializers());

        // auto configure fields
        this.idClasses.forEach(clazz ->
                context.configOverride(clazz)
                        .setIncludeAsProperty(JsonInclude.Value.construct(JsonInclude.Include.NON_EMPTY, null))
        );
    }
}
