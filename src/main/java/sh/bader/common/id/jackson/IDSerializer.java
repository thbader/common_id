package sh.bader.common.id.jackson;

import com.fasterxml.jackson.databind.BeanProperty;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.jsontype.TypeSerializer;
import com.fasterxml.jackson.databind.ser.std.ReferenceTypeSerializer;
import com.fasterxml.jackson.databind.type.ReferenceType;
import com.fasterxml.jackson.databind.util.NameTransformer;
import sh.bader.common.id.ID;

public class IDSerializer extends ReferenceTypeSerializer<ID<?, ?>> {

    private static final long serialVersionUID = -1985338686986969764L;

    public IDSerializer(ReferenceType fullType, boolean staticTyping, TypeSerializer vts, JsonSerializer<Object> ser) {
        super(fullType, staticTyping, vts, ser);
    }

    protected IDSerializer(IDSerializer base, BeanProperty property, TypeSerializer vts, JsonSerializer<?> valueSer, NameTransformer unwrapper, Object suppressableValue, boolean suppressNulls) {
        super(base, property, vts, valueSer, unwrapper, suppressableValue, suppressNulls);
    }

    @Override
    protected ReferenceTypeSerializer<ID<?, ?>> withResolved(BeanProperty prop, TypeSerializer vts, JsonSerializer<?> valueSer, NameTransformer unwrapper) {
        return new IDSerializer(this, prop, vts, valueSer, unwrapper, this._suppressableValue, this._suppressNulls);
    }

    @Override
    public ReferenceTypeSerializer<ID<?, ?>> withContentInclusion(Object suppressableValue, boolean suppressNulls) {
        return new IDSerializer(this, this._property, this._valueTypeSerializer, this._valueSerializer, this._unwrapper, suppressableValue, suppressNulls);
    }

    @Override
    public boolean isEmpty(SerializerProvider provider, ID<?, ?> value) {
        return value == null;
    }

    @Override
    protected boolean _isValuePresent(ID<?, ?> value) {
        return value == null;
    }

    @Override
    protected Object _getReferenced(ID<?, ?> value) {
        return value.getId();
    }

    @Override
    protected Object _getReferencedIfPresent(ID<?, ?> value) {
        return value.getId();
    }
}
