package sh.bader.common.id.spring;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.reflections.Reflections;
import org.springframework.format.Formatter;
import org.springframework.format.FormatterRegistry;
import sh.bader.common.id.IDEntity;
import sh.bader.common.id.ID;

import java.lang.reflect.Modifier;
import java.text.ParseException;
import java.util.Locale;
import java.util.Objects;

/**
 * Formatter for ids.
 *
 * @param <I> the id type.
 */
public class IDFormatter<I extends ID<I, E>, E extends IDEntity<I, E>> implements Formatter<I> {
    private final Class<I> clazz;

    /**
     * Constructs a new id formatter.
     *
     * @param clazz the sid class.
     */
    public IDFormatter(final Class<I> clazz) {
        this.clazz = clazz;
    }

    @Override
    public String print(final I id, final Locale locale) {
        return Objects.toString(id.getId());
    }

    @Override
    public I parse(final String text, final Locale locale) throws ParseException {
        I id = null;

        if (StringUtils.isNotBlank(text)) {
            try {
                id = ID.forIdClass(this.clazz, StringUtils.trimToNull(text));
            } catch (final IllegalArgumentException e) {
                throw new ParseException(e.getMessage(), -1);
            }
        }

        return id;
    }


    public static <I extends ID<I, E>, E extends IDEntity<I, E>> void register(final FormatterRegistry registry, Class<I> baseClass) {
        register(registry, baseClass.getPackage().getName());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        } else if (o == null || this.getClass() != o.getClass()) {
            return false;
        }

        IDFormatter<I, E> that = (IDFormatter<I, E>) o;
        return new EqualsBuilder()
                .append(this.clazz, that.clazz)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .append(this.clazz)
                .toHashCode();
    }

    public static <I extends ID<I, E>, E extends IDEntity<I, E>> void register(final FormatterRegistry registry, String prefix) {
        final Reflections reflections = new Reflections(prefix);

        reflections.getSubTypesOf(ID.class)
                .stream()
                .map(c -> (Class<I>) c)
                .filter(clazz -> !Modifier.isAbstract(clazz.getModifiers()))
                .forEach(c -> registry.addFormatterForFieldType(c, new IDFormatter<>(c)));
    }
}
