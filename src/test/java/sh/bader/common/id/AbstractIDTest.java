package sh.bader.common.id;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

import java.util.UUID;
import java.util.stream.Stream;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

public class AbstractIDTest {

    public static final DummyID ID = new DummyID(UUID.randomUUID());

    @ParameterizedTest
    @MethodSource("provideValidValues")
    public void equalsWithValidValues(Object value) {
        assertEquals(value, ID);
    }

    private static Stream<Arguments> provideValidValues() {
        return Stream.of(
            Arguments.of(new DummyID(ID.getId())),
            Arguments.of(ID)
        );
    }

    @ParameterizedTest
    @MethodSource("provideInvalidValues")
    public void equalsWithInvalidValues(Object value) {
        assertNotEquals(value, ID);
    }

    private static Stream<Arguments> provideInvalidValues() {
        return Stream.of(
            Arguments.of((Object) null),
            Arguments.of(new DummyID(UUID.randomUUID())),
            Arguments.of(new AnotherDummyID(ID.getId()))
        );
    }
}
