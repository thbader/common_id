package sh.bader.common.id;

import java.util.UUID;

import org.apache.commons.lang3.builder.CompareToBuilder;

public class AnotherDummyEntity implements IDEntity<AnotherDummyID, AnotherDummyEntity>
{
    private final AnotherDummyID id;

    public AnotherDummyEntity(UUID uuid) {
        this.id = new AnotherDummyID(uuid);
    }

    @Override
    public AnotherDummyID getId()
    {
        return this.id;
    }

    @Override
    public int compareTo(AnotherDummyEntity entity)
    {
        return new CompareToBuilder()
            .append(this.getId(), entity.getId())
            .toComparison();
    }
}
