package sh.bader.common.id;

import java.util.UUID;

import sh.bader.common.id.hibernate.AbstractID;

public class AnotherDummyID extends AbstractID<AnotherDummyID, AnotherDummyEntity>
{
    public AnotherDummyID(UUID id)
    {
        super(AnotherDummyEntity.class, id);
    }
}
