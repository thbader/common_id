package sh.bader.common.id;

import java.util.UUID;

import org.apache.commons.lang3.builder.CompareToBuilder;

public class DummyEntity implements IDEntity<DummyID, DummyEntity>
{
    private final DummyID id;

    public DummyEntity(UUID uuid) {
        this.id = new DummyID(uuid);
    }

    @Override
    public DummyID getId()
    {
        return this.id;
    }

    @Override
    public int compareTo(DummyEntity entity)
    {
        return new CompareToBuilder()
            .append(this.getId(), entity.getId())
            .toComparison();
    }
}
