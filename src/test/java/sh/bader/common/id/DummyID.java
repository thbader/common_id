package sh.bader.common.id;

import java.util.UUID;

import sh.bader.common.id.hibernate.AbstractID;

public class DummyID extends AbstractID<DummyID, DummyEntity>
{
    public DummyID(UUID id)
    {
        super(DummyEntity.class, id);
    }
}
