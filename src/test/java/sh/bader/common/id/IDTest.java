package sh.bader.common.id;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.nullValue;

import java.util.UUID;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class IDTest
{
    @Test
    public void forEntityClass() {
        UUID uuid = UUID.randomUUID();

        DummyID id = ID.forEntityClass(DummyEntity.class, uuid);
        assertThat(id, allOf(
            hasProperty("class", equalTo(DummyID.class)),
            hasProperty("id", equalTo(uuid))
        ));
    }

    @Test
    public void forEntityClassWithClassNull() {
        Assertions.assertThrows(NullPointerException.class , () ->ID.forEntityClass(null, UUID.randomUUID()));
    }

    @Test
    public void forEntityClassWithUUIDNull() {
        UUID uuid = UUID.randomUUID();

        DummyID id = ID.forEntityClass(DummyEntity.class, (UUID)null);
        assertThat(id, nullValue());
    }

    @Test
    public void forEntityClassWithInvalidClass() {
        Assertions.assertThrows(
            IllegalArgumentException.class ,
            () ->ID.forEntityClass((Class)String.class, UUID.randomUUID())
        );
    }

    @Test
    public void forIdClass() {
        Class<DummyID> clazz = DummyID.class;
        UUID uuid = UUID.randomUUID();

        DummyID id = ID.forIdClass(clazz, uuid);
        assertThat(id, allOf(
            hasProperty("class", equalTo(clazz)),
            hasProperty("id", equalTo(uuid))
        ));
    }

    @Test
    public void forIdClassWithIdNull() {
        Class<DummyID> clazz = DummyID.class;

        DummyID id = ID.forIdClass(clazz, (UUID)null);
        assertThat(id, nullValue());
    }

    @Test
    public void forIdClassWithClassNull() {
        Assertions.assertThrows(
            NullPointerException.class ,
            () ->ID.forIdClass(null, UUID.randomUUID())
        );
    }

    @Test
    public void forIdClassWithGenericClass() {
        Class<?> clazz = DummyID.class;
        UUID uuid = UUID.randomUUID();

        Object id = ID.forIdClass((Class)clazz, uuid);
        assertThat(id, allOf(
            hasProperty("class", equalTo(clazz)),
            hasProperty("id", equalTo(uuid))
        ));
    }

    @Test
    public void forIdClassWithInvalidClass() {
        Assertions.assertThrows(
            IllegalArgumentException.class ,
            () ->ID.forIdClass((Class) SimilarID.class, UUID.randomUUID())
        );
    }

    private static class SimilarID {
    }
}
