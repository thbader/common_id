package sh.bader.common.id.jackson;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasProperty;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.UUID;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.exc.InvalidFormatException;

import sh.bader.common.id.DummyEntity;
import sh.bader.common.id.DummyID;

public class IDModuleTest
{
    private ObjectMapper objectMapper;

    @BeforeEach
    public void before() {
        this.objectMapper = new ObjectMapper()
            .registerModule(new IDModule(DummyID.class));
    }

    @Test
    public void serializeWithId() throws Exception {
        // given
        Example example = new Example();
        example.setId(new DummyID(UUID.randomUUID()));

        // when
        String json = this.objectMapper.writeValueAsString(example);

        // then
        assertThat(json, equalTo("{\"id\":\"" + example.getId().getId() + "\"}"));
    }

    @Test
    public void serializeWithoutId() throws Exception {
        // given
        Example example = new Example();
        example.setId(null);

        // when
        String json = this.objectMapper.writeValueAsString(example);

        // then
        assertThat(json, equalTo("{}"));
    }

    @Test
    public void deserializeWithValidID() throws Exception {
        // given
        UUID id = UUID.randomUUID();
        String json = "{\"id\":\"" + id + "\"}";

        // when
        Example result = this.objectMapper.readValue(json, Example.class);

        // then
        assertThat(result, hasProperty("id", allOf(
            hasProperty("id", equalTo(id)),
            hasProperty("entityClass", equalTo(DummyEntity.class))
        )));
    }

    @Test
    public void deserializeWithInvalidID() {
        // given
        UUID id = UUID.randomUUID();
        String json = "{\"id\":\"" + id + "-xyz\"}";

        // when
        // then
        assertThrows(InvalidFormatException.class, () -> this.objectMapper.readValue(json, Example.class));
    }

    public static class Example {
        @JsonProperty("id")
        private DummyID id;

        public DummyID getId()
        {
            return id;
        }

        public void setId(DummyID id)
        {
            this.id = id;
        }
    }
}
